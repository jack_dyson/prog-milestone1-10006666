﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your name: ");
            var name = Console.ReadLine();

            Console.WriteLine("Please enter your age: ");
            var age = int.Parse(Console.ReadLine());

            Console.WriteLine($"Your name is {name} and you are {age}");
            Console.ReadKey(true);
            Console.WriteLine("Your name is {0} and you are {1} years old.", name, age);
            Console.ReadKey(true);
            Console.WriteLine("Your name is " + name + " and you are " + age + " years old");

            
        }
    }
}
