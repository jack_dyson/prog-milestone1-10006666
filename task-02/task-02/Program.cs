﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Would you please enter you birth month: ");
            var a = Console.ReadLine();

            Console.WriteLine("And the date (1-31): ");
            var b = int.Parse(Console.ReadLine());

            Console.WriteLine($"You were born on the {b}th of {a}");
        }
    }
}
