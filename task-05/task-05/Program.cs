﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_05
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Please enter the current time to convert from 24hr to am/pm (1-24): ");
            var a = double.Parse(Console.ReadLine());

            if (a > 12)
            {
                Console.WriteLine($"The time is {a - 12}pm");
            }
            else
            {
                Console.WriteLine($"The time is {a}am");
            }
        }
    }
}
