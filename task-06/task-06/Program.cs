﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_06
{
    class Program
    {
        static void Main(string[] args)
        {
            var num = new List<int>();
            var counter = 5;
            int i;

            for (i = 0; i < counter; i++)
            {
                Console.WriteLine("Please enter a number: ");
                num.Add(int.Parse(Console.ReadLine()));
            }
            Console.WriteLine("The sum of your inputs is: " + num.Sum(x => Convert.ToInt32(x)));
        }
    }
}
