﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_08
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 0;
            int i;

            Console.WriteLine("Ready for countdown.");
            Console.ReadKey(true);
            for (i = 99; i > counter; i--)
            {
                Console.WriteLine($"{i}");
            }
        }
    }
}
