﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_09
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Here are the leap years for the next 20 years.");

            for (var a = 2016; a <= 2036; a += 4)
            {
                Console.WriteLine($"{a}"); 
            }

        }
    }
}
