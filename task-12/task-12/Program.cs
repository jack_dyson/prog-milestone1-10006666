﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number to see if it's odd or even: ");
            var a = double.Parse(Console.ReadLine());

            if (a % 2 == 0)
            {
                Console.WriteLine("This number is even");
            }
            else
            {
                Console.WriteLine("This number is odd");
            }

        }
    }
}
