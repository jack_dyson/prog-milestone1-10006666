﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word: ");
            var w = Console.ReadLine();
            var count = w.Length;

            Console.WriteLine($"Your word, {w}, is {count} letters long.");
        }
    }
}
