﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new List<Tuple<string, int>>();
            a.Add(Tuple.Create("Jim", 32));
            a.Add(Tuple.Create("Don", 46));
            a.Add(Tuple.Create("Larry", 28));

            Console.WriteLine(a[0].Item1);
            Console.WriteLine(a[0].Item2);
            Console.WriteLine("\n");
            Console.WriteLine(a[1].Item1);
            Console.WriteLine(a[1].Item2);
            Console.WriteLine("\n");
            Console.WriteLine(a[2].Item1);
            Console.WriteLine(a[2].Item2);
        }
    }
}
