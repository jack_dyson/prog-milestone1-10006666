﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var d = new Dictionary<string, string>();
            var l = new List<string>();
            var l2 = new List<string>();

            d.Add("Kumara", "Vegetable");
            d.Add("Banana", "Fruit");
            d.Add("Apple", "Fruit");
            d.Add("Broccoli", "Vegetable");
            d.Add("Carrot", "Vegetable");

            foreach (var i in d)
            {
                if(i.Value == "Fruit")
                {
                    l.Add(i.Key);
                }
                if (i.Value == "Vegetable")
                {
                    l2.Add(i.Key);
                }
            }

            Console.WriteLine($"There are {l.Count} fruits, and {l2.Count} vegetables.");
            Console.WriteLine($"The fruits are: ");
            l.ForEach(Console.WriteLine);
               
        }
    }
}
