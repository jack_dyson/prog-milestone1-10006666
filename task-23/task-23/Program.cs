﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_23
{
    class Program
    {
        static void Main(string[] args)
        {
            var dict = new Dictionary<string, string>();

            dict.Add("Monday", "Week day");
            dict.Add("Tuesday", "Week day");
            dict.Add("Wednesday", "Week day");
            dict.Add("Thursday", "Week day");
            dict.Add("Friday", "Week day");
            dict.Add("Saturday", "Weekend");
            dict.Add("Sunday", "Weekend");

            foreach (var a in dict)
            {
                Console.WriteLine($"{a.Key}, {a.Value}");
            }
        }
    }
}
