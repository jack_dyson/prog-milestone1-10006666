﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_24
{
    class Program
    {
        static void Main(string[] args)
        {
            //I like switch/case
            var b = "";

            do
            {
                Console.WriteLine("----------------");
                Console.WriteLine("-- 1. First");
                Console.WriteLine("-- 2. Second");
                Console.WriteLine("-- 3. Third");
                Console.WriteLine("----------------");

                Console.WriteLine("Welcome, Please enter your selection: ");
                var a = int.Parse(Console.ReadLine());

                Console.Clear();
                switch (a)
                {
                    case 1:
                        Console.WriteLine("Blah");

                        Console.WriteLine("Press m/M to go back to menu.");
                        b = Console.ReadLine();
                        Console.Clear();
                        break;
                    case 2:
                        Console.WriteLine("Blah");

                        Console.WriteLine("Press m/M to go back to menu.");
                        b = Console.ReadLine();
                        Console.Clear();
                        break;
                    case 3:
                        Console.WriteLine("Blah");

                        Console.WriteLine("Press m/M to go back to menu.");
                        b = Console.ReadLine();
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Please enter a /actual/ selection.");
                        Console.WriteLine("Press m/M to go back to menu.");
                        b = Console.ReadLine();
                        Console.Clear();
                        break;
                }
            } while ((b == "m" || b == "M"));
        }
    }
}
