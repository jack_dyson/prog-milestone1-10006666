﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_25
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome, please enter a number: ");
            bool isNumeric;
            int i = 0;
            var a = (Console.ReadLine());

            //if a number is input, this is skipped
            //this is to prevent crashing from a string input
            isNumeric = int.TryParse(a, out i);
            if (isNumeric == false)
            {
                Console.WriteLine("Please enter a number, not a string.");
            }
            //for this
            else
            {
                Console.WriteLine($"You entered: {a}");
            }  
        }
    }
}
