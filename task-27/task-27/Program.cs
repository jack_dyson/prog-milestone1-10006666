﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_27
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new string[5] { "Red", "Blue", "Yellow", "Green", "Pink" };

            Array.Sort(a);

            Console.WriteLine(string.Join(",", a));
        }
    }
}
