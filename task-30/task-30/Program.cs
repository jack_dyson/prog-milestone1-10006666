﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_30
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number: ");
            var a = (Console.ReadLine());
            Console.WriteLine("And again: ");
            var b = (Console.ReadLine());

            var ab = a + b;

            Console.WriteLine($"This is what 2 strings added looks like: {ab}");

            var c = int.Parse(a);
            var d = int.Parse(b);

            Console.WriteLine($"This is what two ints added looks like: {c + d}");
        }
     }
}
