﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_33
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the number of students: ");
            var stu = int.Parse(Console.ReadLine());

            var group = stu / 28;

            if (stu < 28)
            {
                group = 1;
            }
            if (stu > 28)
            {
                group++;
            }
            Console.WriteLine($"Groups needed for {stu} students is {group}");
        }
    }
}
