﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_35
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number: ");
            var num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("And another: ");
            var num2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Last one: ");
            var num3 = int.Parse(Console.ReadLine());

            var eq1 = num1 + num2 + num3;
            var eq2 = num1 * num3 / num2;
            var eq3 = (num3 * num2) + num1;
            var eq4 = num1 / (num2 % num3);
            var eq5 = num2 * num2 * num3;

            Console.WriteLine("-- 1. equation 1");
            Console.WriteLine("-- 2. equation 2");
            Console.WriteLine("-- 3. equation 3");
            Console.WriteLine("-- 4. equation 4");
            Console.WriteLine("-- 5. equation 5");
            Console.WriteLine("Please choose an equation: ");
            var input = int.Parse(Console.ReadLine());

            switch (input)
            {
                case 1:
                    Console.WriteLine($"{eq1}");
                    break;
                case 2:
                    Console.WriteLine($"{eq2}");
                    break;
                case 3:
                    Console.WriteLine($"{eq3}");
                    break;
                case 4:
                    Console.WriteLine($"{eq4}");
                    break;
                case 5:
                    Console.WriteLine($"{eq5}");
                    break;
                default:
                    Console.WriteLine("Please make a selection.");
                    break;
            }
        }
    }
}
