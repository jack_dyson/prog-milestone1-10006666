﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_36
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = 10;

            for (var i = 0; i <= count; i++)
            {
                if (i == count)
                {
                    Console.WriteLine("The index is now equal to the counter.");
                }
                else
                {
                    Console.WriteLine("The counter and index are currently inequal.");
                }
            }
        }
    }
}
